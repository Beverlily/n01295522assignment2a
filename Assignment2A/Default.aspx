﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2A._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!--code based off default template-->
    <div class="jumbotron">
        <h1 id="home-header">Beverly's Study Guide</h1>
        <p class="lead">This website was created as a study guide for my upcoming midterms. It covers some concepts in my Web Development courses that I've found tricky.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>JavaScript</h2>
            <p>
                A for loop in JavaScript is used to run a piece of code for a certain number of iterations as long as the condition set is true.
            </p>
            <p>
                <a class="btn btn-default" runat="server" href="~/JavaScript">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>SQL</h2>
            <p>
                A <code>INNER JOIN</code> is useful as it connects two related tables based on matching values they have. In SQL, an <code>INNER JOIN</code> connects two tables based on a column and returns all the matching rows.
            </p>
            <p>
                <a class="btn btn-default" runat="server" href="~/SQL">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>HTML/CSS</h2>
            <p>
                The margin of a HTML element is the amount of space it has outside of its border. On the other hand, the padding of a HTML is the amount of space the element has between its content and its border.
            </p>
            <p>
                <a class="btn btn-default" runat="server" href="~/HTMLCSS">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
