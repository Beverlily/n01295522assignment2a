﻿<%@ Page Title="HTML/CSS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTMLCSS.aspx.cs" Inherits="Assignment2A.HTMLCSS" %>
<asp:Content ID="HTMLCSSContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
      <h1>HTML/CSS: Margin vs Padding</h1>
      <p>The margin of a HTML element is the amount of space it has outside of its border. On the other hand, the padding of a HTML is the amount of space the element has between its content and its border.</p>
   </div>
</asp:Content>
<asp:Content ID="MyMarginPaddingExample" ContentPlaceHolderID="MyExample" runat="server">
   <div>
      <h2>My Margin and Padding Example</h2>
      <div>
         <p id="exampleA">Hi I'm paragraph A, formed using the paragraph element tag 'p'. Right now I have default margin and padding.</p>
         <p id="exampleB">Hi I'm paragraph B, formed using the paragraph element tag 'p'. Right now I have default margin and padding.</p>
      </div>
      <h3>HTML:</h3>
      <pre class="pre-dark"><code>&lt;p id="exampleA"&gt;Hi I'm paragraph A, formed using the paragraph element tag 'p'. Right now I have default margin and padding.&lt;p&gt;

&lt;p id="exampleB"&gtHi I'm paragraph B, formed using the paragraph element tag 'p'. Right now I have default margin and padding.&lt;p&gt;</code></pre>
      <h3>CSS:</h3>
      <pre class="pre-dark"><code>#exampleA {
    border: 1px solid blue;

#exampleB {
    border: 1px solid blue;
}</code></pre>
      <br />
      <p> Right now, both paragraph A and B have the default margin and padding. Recall that margin is the amount of space an element has outside its border.</p>
      <p> So, if I want to add space between the two elements paragraph A and paragraph B, I could increase their margin.</p>
      <p> Lets increase both their margins by 50px by adding in the CSS <code>margin: 50px;</code> </p>
      <pre class="pre-dark"><code>#exampleA{
    border: 1px solid blue;
    margin: 50px;
}

#exampleB{
    border: 1px solid blue;
    margin: 50px;
}</code></pre>
      <div>
         <p id="margin-exampleA">Hi I'm paragraph A, formed using the paragraph element tag 'p'. My margin is now 50px.</p>
         <p id="margin-exampleB">Hi I'm paragraph B, formed using the paragraph element tag 'p'. My margin is now 50px.</p>
      </div>
      <p>Now, you can see that there's 50px of space around all sides of paragraph A and paragraph B.</p>
      <p>What if I increase the padding? Recall that the padding of an element is the space between an elements content and its border.</p>
      <p>So let's increase the padding of paragraph A to 50px by adding in the CSS <code>padding: 50px;</code></p>
      <p>Lets also add padding to the top of paragraph B by adding in the CSS <code>padding-top: 50px</code></p>
      <pre class="pre-dark"><code>#exampleA {
    border: 1px solid blue;
    margin: 50px;
    padding: 50px;
}

exampleB {
    border: 1px solid blue;
    margin: 50px;
    padding-top: 50px;
}</code></pre>
      <div>
         <p id="padding-exampleA">Hi I'm paragraph A, formed using the paragraph element tag 'p'. My margin is now 50px and my padding is 50px.</p>
         <p id="padding-exampleB">Hi I'm paragraph B, formed using the paragraph element tag 'p'. My margin is now 50px and my top padding is 50px.</p>
      </div>
      <p>As you can see, by adding <code>padding: 50px;</code> to paragraph A, there is now 50px between the content of paragraph A and its border.</p>
      <p>In paragraph B, since we specified the area where we want padding by adding <code>padding-top: 50px;</code>, there is 50px between the content of paragraph B and its border only on the top side of the content.</p>
   </div>
</asp:Content>
<asp:Content ID="WebMarginPaddingSExample" ContentPlaceHolderID="WebExample" runat="server">
   <div>
      <h2>Web Example: Margin</h2>
      <p>Here is another example of margin from <a href="https://www.w3schools.com/Css/css_margin.asp">W3Schools</a>.</p>
      <div id="w3-margin-example">This div element has a top margin of 100px, a right margin of 150px, a bottom margin of 100px, and a left margin of 80px.</div>
      <h3>HTML:</h3>
      <pre class="pre-dark"><code>&lt;div&gt;This div element has a top margin of 100px, a right margin of 150px, a bottom margin of 100px, and a left margin of 80px.&lt;div&gt;</code></pre>
      <h3>CSS:</h3>
      <pre class="pre-dark"><code>div {
    border: 1px solid black;
    margin-top: 100px;
    margin-bottom: 100px;
    margin-right: 150px;
    margin-left: 80px;
    background-color: lightblue;
}</code></pre>
       <p>In this example, they've created a div element with different margin values for the top, bottom, left, and right side of the element.</p>
       <p>The div element they've created has 100px of space outside its border on the top side, 100px on the bottom side, 150px on the right side, and 80px on the left side. </p>
   </div>
    <br />
   <div>
      <h2>Web Example: Padding</h2>
      <p>Here is another example of padding, also from <a href="https://www.w3schools.com/csS/css_padding.asp">W3Schools</a>.</p>
      <div id="w3-padding-example">This element has a padding of 70px.</div>
       <h3>HTML:</h3>
       <pre class="pre-dark"><code>&lt;div&gt;This element has a padding of 70px.&lt;div&gt;</code></pre>

       <h3>CSS:</h3>
       <pre class="pre-dark"><code>div {
    padding: 70px;
    border: 1px solid #4CAF50;
}</code></pre>
       <p>In this example, they've made a div element with a padding of 70px. This means that between the content of this div and its border, there is 70px all around.</p>
   </div>
</asp:Content>
<asp:Content ID="UsefulHTMLCSSLinks" ContentPlaceHolderID="UsefulLinks" runat="server">
   <div class="links-container">
      <h2>More Learning Resources:</h2>
      <ul>
         <li><a href="https://medium.com/frontendshortcut/margin-vs-padding-c1fc8ea8bfaf">Medium</a></li>
         <li><a href="http://www.goer.org/HTML/intermediate/margins_and_padding/">The Pocket HTML Tutorial</a></li>
         <li><a href="https://www.digizol.com/2006/12/margin-vs-padding-css-properties.html">Digizol</a></li>
      </ul>
   </div>
</asp:Content>