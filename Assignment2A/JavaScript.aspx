﻿<%@ Page Title="JavaScript" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="Assignment2A.JavaScript" %>
<asp:Content ID="JSContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
      <h1>JavaScript: For Loops</h1>
      <p>A for loop in JavaScript is used to run a piece of code for a certain number of iterations as long as the condition set is true.</p>
      <div>
         <p>A for loop has 3 parts:</p>
         <ol>
            <li>The first part of a for loop states the starting value of the counter. This occurs before the loop.</li>
            <li>The second part states the condition which must be true for the code inside the loop to run.</li>
            <li>The third part states the increment value of the counter. This part occurs after the code inside the loop runs.</li>
         </ol>
      </div>
   </div>
</asp:Content>
<asp:Content ID="MyJSExample" ContentPlaceHolderID="MyExample" runat="server">
   <div>
      <h2>My Example</h2>
      <p>I've written a piece of code that demonstrates a for loop:</p>
      <pre class="pre-dark"><code>var sum=0;

for(var counter=1; counter<3; counter++){
    sum=sum+1;
    alert(sum);
}
alert("Total sum is: " + sum);
</code></pre>
      <h3>Output of the code:</h3>
      <pre><code>1
2
Total sum is: 2
</code></pre><br />
      <div>
         <p>In my example, I created a variable called <code>sum</code> that has a starting value of 0. </p>
         <p>For Part 1) of my loop, <code>var counter=1</code>, I've created a variable named <code>counter</code> and set it equal to 1. This means that the counter for this loop has a starting value of 1.</p>
         <p>For Part 2) of my loop, I've set the condition as <code>counter<3</code>. This means that the code inside my loop will run as long as the counter is less 3.</p>
         <p>For Part 3) of my loop, <code>counter++</code>, I've set the counter to increase by 1 every time after the code in the loop has executed.</p>
      </div>
      <br />
      <h3>A Walkthrough of My Code</h3>
      <div>
         <p>After you run this code, the counter will be set to 1. Then, the condition <code>counter<3</code> will be checked.
         <p>
         <p>The counter value is 1, so the condition <code>counter<3</code> is true.</p>
         <p>Then the code in the loop will run. It will execute <code>sum=sum+1;</code> which would be <code>sum=0+1;</code>, which means <code>sum=1;</code>. The line <code>alert(sum);</code> will then execute and print out 1.</p>
         <p>After the code in the loop has executed, the Part 3) of the loop, <code>counter++</code>, will execute. This means that the counter is now 2.</p>
         <p>The condition will then be checked again. The new value of the counter is 2, so the condition <code>counter<3</code> is still true. The code in the loop will execute because the condition is true.</p>
         <p>Inside the loop, it will execute <code>sum=sum+1;</code> again. Which means <code>sum=1+1;</code>, so <code>sum=2;</code>. The <code>alert(sum);</code> will then print out 2.</p>
         <p>After the loop has executed, Part 3) of the loop, <code>counter++</code>, will execute. The new value of the counter is 3.</p>
         <p>The condition <code>counter<3</code> will be checked again. Since the counter value is 3, the condition <code>counter<3</code> is false. Thus, the code inside the loop will not run and the loop stops.</p>
         <p>The code after the loop, <code>alert("Total sum is: " + sum);</code> will then run and print "Total sum is: 2".</p>
      </div>
   </div>
</asp:Content>
<asp:Content ID="WebJSExample" ContentPlaceHolderID="WebExample" runat="server">
   <div>
      <h2>Example From the Web</h2>
      <p>The code below is another example of a for loop found on <a href="http://www.tutorialsteacher.com/javascript/javascript-for-loop">TutorialsTeacher</a>.</p>
      <pre class="pre-dark"><code>var arr = [10, 11, 12, 13, 14];

for (var i = 0; i < 5; i++)
{
    console.log(arr[i]);
}</code></pre>
      <h3>Output of the code:</h3>
      <pre><code>10
11
12
13
14</code></pre><br />
      <p>For this code, the counter starts at 0.</p>
      <p>The condition is then checked. If the counter is less than 5, the code inside the loop will run.</p>
      <p>If the condition is true and the code in the loop runs, the counter increases by 1.</p>
      <p>Thus, the loop will run 5 times and the elements of the array named <code>arr</code> from indexs 0 to 4 will be printed.</p>
   </div>
</asp:Content>
<asp:Content ID="UsefulJSLinks" ContentPlaceHolderID="UsefulLinks" runat="server">
   <div class="links-container">
      <h2>More Learning Resources:</h2>
      <ul>
         <li><a href="https://www.learn-js.org/en/Loops">LearnJS</a></li>
         <li><a href="https://www.w3schools.com/js/js_loop_for.asp">W3Schools</a></li>
         <li><a href="https://www.tutorialspoint.com/javascript/javascript_for_loop.htm">TutorialsPoint</a></li>
         <li><a href="https://www.dofactory.com/tutorial/javascript-loops">DoFactory</a></li>
         <li><a href="https://www.digitalocean.com/community/tutorials/how-to-construct-for-loops-in-javascript">DigitalOcean</a></li>
         <li><a href="https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/">GeeksforGeeks</a></li>
      </ul>
   </div>
</asp:Content>