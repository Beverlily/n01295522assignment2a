﻿<%@ Page Title="SQL" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="Assignment2A.SQL" %>
<asp:Content ID="SQLContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
      <h1>SQL: Inner Join</h1>
      <p>A <code>INNER JOIN</code> is useful as it connects two related tables based on matching values they have. In SQL, an <code>INNER JOIN</code> connects two tables based on a column and returns all the matching rows.</p>
   </div>
</asp:Content>
<asp:Content ID="MyJoinExample" ContentPlaceHolderID="MyExample" runat="server">
   <div>
      <h2>My Example</h2>
      <div>
         <table>
            <caption>Student</caption>
            <tr>
               <th>Student_ID</th>
               <th>Student_Name</th>
            </tr>
            <tr>
               <td>1</td>
               <td>Beverly Li</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Victor Smith</td>
            </tr>
            <tr>
               <td>3</td>
               <td>Vivian Brooks</td>
            </tr>
         </table>
         <table>
            <caption>Grade</caption>
            <tr>
               <th>StudentID</th>
               <th>Course_Name</th>
               <th>Course_Mark</th>
            </tr>
            <tr>
               <td>1</td>
               <td>Web Programming</td>
               <td>80</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Web Programming</td>
               <td>83</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Database</td>
               <td>92</td>
            </tr>
            <tr>
               <td>3</td>
               <td>Database</td>
               <td>76</td>
            </tr>
            <tr>
               <td>3</td>
               <td>Web Programming</td>
               <td>65</td>
            </tr>
         </table>
      </div>
      <br />
      <div>
         <p>Let's say there's two tables:</p>
         <ol>
            <li>A Student table that stores a student's ID number and the students name.</li>
            <li>A Grade table that stores a student's ID number, a course name, and a course grade.</li>
         </ol>
         <p>If you want to find the name of students who got above 70 in the Web Programming course, you cannot find this information by just looking at only one table. We have to look at both the student table and grade table. </p>
         <p>We can make a connectiion between the Student table to the Grade table based on their common column, <code>Student_ID</code>.</p>
         <br />
         <p>To do this, we will use <code>INNER JOIN</code>.</p>
         <pre class="pre-dark"><code>SELECT *
FROM student
INNER JOIN grade
ON student.student_id = grade.student_id
ORDER BY student.student_id ASC</code></pre>
         <p>This code will join the student table to the grade table and return rows where the <code>Student</code> table's <code>Student_ID</code> matches with the <code>Grade</code> table's <code>Student_ID</code></p>
         <br />
         <p>The result will be this:</p>
         <table>
            <tr>
               <th>Student_ID</th>
               <th>Student_Name</th>
               <th>Course_Name</th>
               <th>Course_Mark</th>
            </tr>
            <tr>
               <td>1</td>
               <td>Beverly Li</td>
               <td>Web Programming</td>
               <td>80</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Victor Smith</td>
               <td>Web Programming</td>
               <td>83</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Victor Smith</td>
               <td>Database</td>
               <td>92</td>
            </tr>
            <tr>
               <td>3</td>
               <td>Vivian Brooks</td>
               <td>Database</td>
               <td>76</td>
            </tr>
            <tr>
               <td>3</td>
               <td>Vivian Brooks</td>
               <td>Web Programming</td>
               <td>65</td>
            </tr>
         </table>
         <br />
         <p>Now, with this joined table, we can easily find the name of students who got a course mark above 70 in the Web Programming course.</p>
         <p>To do this we just have to filter out the rows in this joined table by adding <code>WHERE</code> statement:</p>
         <pre class="pre-dark"><code>SELECT *
FROM student
INNER JOIN grade
ON student.student_id = grade.student_id
ORDER BY student.student_id ASC
WHERE course_mark>70 AND course_name = 'Web Programming'</code></pre>
         <br />
         <p>The result will be this:</p>
         <table>
            <tr>
               <th>Student_ID</th>
               <th>Student_Name</th>
               <th>Course_Name</th>
               <th>Course_Mark</th>
            </tr>
            <tr>
               <td>1</td>
               <td>Beverly Li</td>
               <td>Web Programming</td>
               <td>80</td>
            </tr>
            <tr>
               <td>2</td>
               <td>Victor Smith</td>
               <td>Web Programming</td>
               <td>83</td>
            </tr>
         </table>
      </div>
   </div>
</asp:Content>
<asp:Content ID="WebJoinExample" ContentPlaceHolderID="WebExample" runat="server">
   <div>
      <h2>Web Example</h2>
      <p>Here is another example from the website <a href="http://www.sql-join.com/sql-join-types/">SQL-Join</a>.</p>
      <div>
         <table>
            <caption>Customers</caption>
            <tr>
               <th>first_name</th>
               <th>last_name</th>
               <th>order_date</th>
               <th>order_amount</th>
            <tr>
               <td>George</td>
               <td>Washington</td>
               <td>07/4/1776</td>
               <td>$234.56</td>
            </tr>
            <tr>
               <td>John</td>
               <td>Adams</td>
               <td>05/23/1784</td>
               <td>$124.00</td>
            </tr>
            <tr>
               <td>Thomas</td>
               <td>Jefferson</td>
               <td>03/14/1760</td>
               <td>$78.50</td>
            </tr>
            <tr>
               <td>Thomas</td>
               <td>Jefferson</td>
               <td>09/03/1790</td>
               <td>$65.50</td>
            </tr>
         </table>
         <table>
            <caption>Orders</caption>
            <tr>
               <th>order_id</th>
               <th>order_date</th>
               <th>amount</th>
               <th>customer_id</th>
            </tr>
            <tr>
               <td>1</td>
               <td>07/04/1776</td>
               <td>$234.56</td>
               <td>1</td>
            </tr>
            <tr>
               <td>2</td>
               <td>03/14/1760</td>
               <td>$78.50</td>
               <td>3</td>
            </tr>
            <tr>
               <td>3</td>
               <td>05/23/1784</td>
               <td>$124.00</td>
               <td>2</td>
            </tr>
            <tr>
               <td>4</td>
               <td>09/03/1790</td>
               <td>$65.50</td>
               <td>3</td>
            </tr>
            <tr>
               <td>5</td>
               <td>07/21/1795</td>
               <td>$25.50</td>
               <td>10</td>
            </tr>
            <tr>
               <td>6</td>
               <td>11/27/1787</td>
               <td>$14.40</td>
               <td>9</td>
            </tr>
         </table>
      </div>
      <br />
      <p>In their example, they decided to join the tables because they wanted to get a list of customers who placed an order and the details of the order they placed.</p>
      <br />
      <p>To do this, they used an <code>INNER JOIN</code></p>
      <pre class="pre-dark"><code>SELECT first_name, last_name, order_date, order_amount
FROM customers c
INNER JOIN orders o
ON c.customer_id = o.customer_id</code></pre>
      <br />
      <p>This returned the table:</p>
      <table>
         <tr>
            <th>first_name</th>
            <th>last_name</th>
            <th>order_date</th>
            <th>order_amount</th>
         <tr>
            <td>George</td>
            <td>Washington</td>
            <td>07/4/1776</td>
            <td>$234.56</td>
         </tr>
         <tr>
            <td>John</td>
            <td>Adams</td>
            <td>05/23/1784</td>
            <td>$124.00</td>
         </tr>
         <tr>
            <td>Thomas</td>
            <td>Jefferson</td>
            <td>03/14/1760</td>
            <td>$78.50</td>
         </tr>
         <tr>
            <td>Thomas</td>
            <td>Jefferson</td>
            <td>09/03/1790</td>
            <td>$65.50</td>
         </tr>
      </table>
   </div>
</asp:Content>
<asp:Content ID="UsefulJoinLinks" ContentPlaceHolderID="UsefulLinks" runat="server">
   <div class="links-container">
      <h2>More Learning Resources</h2>
      <ul>
         <li><a href="http://www.sql-join.com/sql-join-types/">SQL-Join</a></li>
         <li><a href="https://www.w3schools.com/sql/sql_join_inner.asp">W3Schools</a></li>
         <li><a href="https://www.tutorialspoint.com/sql/sql-inner-joins.htm">TutorialsPoint</a></li>
         <li><a href="https://community.modeanalytics.com/sql/tutorial/sql-inner-join/">Mode</a></li>
         <li><a href="http://www.mysqltutorial.org/mysql-inner-join.aspx">MySQL Tutorial</a></li>
      </ul>
   </div>
</asp:Content>